# pythondjango_restapi_react

A blog like app with backend and frontend

## Getting Started

    code
$ Git clone https://gitlab.com/officerebel/pythondjango_restapi_react.git

$ cd pythondjango_restapi_react/backend	

### Open two terminal windows
one for frontend and one for backend

### the Backend

```
Make a virtual environment
$virtualenv  django
$ source django/bin/activate

$ Install the requirements
$ pip install -r requirements.txt 

```

### set the secret key

```
go to /todo_api/settings.py and set the secret key

SECRET_KEY = 'here my secret key'
```
### Activate the backend server
```
$ python3 manage.py runserver
```

### Activate the frontend server

```

yarn install
yarn start
open browser http://localhost:3000


```







## Built With

* [Django](https://www.djangoproject.com) - The web framework used
* [React](https:/https://reactjs.org) - The web framework used

## Authors

* **Tim Vogt** - *Initial work* - [Officerebel](https://gitlab.com/officerebel)



## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

